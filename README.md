Online learning resource manager (integrated with Facebook share for groups/pages)

INTRODUCTION

The web application will allow users to access courses in which they have been enrolled by their professors or that they have enrolled in by themselves from anywhere they have an internet connection.
The web application will allow users to interact via a comment section available at the end of the course and at the end of each individual lecture.
The web application will have 2 types of users: student and proffesors with different type of priviliges.

USER BENEFITS

The login will be based on the facebook login api or in the case the user doesnt have a facebook account there will be a signup form and a regular login form.
The student user should be able to search through various available courses.
The student user should be able to see upcoming deadlines for homeworks on each course.
The student user should be able to leave questions at the end of each lecture for the proffesor and also be able to leave a comment visible to the other student users.
The student user should be able to upload homeworks to the courses he is enrolled in.
The student user should be able to see its progress on the courses and also his grades.
The proffesor user should be able to upload course materials in various formats (doc, pdf, csv, ppt and so on)
The proffesor user should be able to grade homeworks uploaded by the student users.
The proffesor user should be able to set deadlines for homeworks and also for varius tests or exams.
The proffesor user should be able to share its courses on facebook groups via the facebook share api.
The proffesor user should be able to share deadlines on facebook groups via the facebook share api.

IMPLEMENTATION

The first page of the web application is going to be a login form where the user can choose between facebook login or a form based authentification.
If the user has neither a facebook account nor an website account they will be directed to a signup form.
After the login page, users will have a different view based on their status, proffesor or student.
Student users will see all their courses in a list with their progress and their deadlines (if they exist) attached.
Proffesor users will see their courses and the number of students that are enrolled in each course.
By clicking on the courses the Student users should be redirected to a page where all the lectures from that course are available as links.
The proffesor will see the same page but with the added functionality of uploading documents to each lecture and also the privilege to set homeworks and deadlines.
After setting a deadline the professor will be able to share it on facebook via facebook share.



