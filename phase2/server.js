const express =require("express")
const bodyParser=require("body-parser")
const Sequelize=require("sequelize")

const sequelize=new Sequelize('REST_APP', 'root', '',
    {dialect:'mysql',
    define:{
        timestamps:false
    }
})
//                                name  def config
const Student=sequelize.define('student', {
    name:{
        type: Sequelize.STRING,
        allowNull:false,
        validate:{
            len:[3,40]
        }
    },
    email:{
        type:Sequelize.STRING,
        allowNull:false,
        validate:{
            isEmail:true
        }
    },
    faculty:{
        type:Sequelize.STRING,
        allowNull:false,
        validate:{
            len:[3,7] //acronyms
        }
    },
},{
    underscored :true 
})

const Professor=sequelize.define('professor', {
   pfirst_name : {  //professor first_name
		type : Sequelize.STRING,
		allowNull : false,
		validate : {
			len : [3,30]
		},
	/*	set(value){
			this.setDataValue('first_name', value.toLowerCase())
		}
		*/
	},
	plast_name : {   //professor last_name
		type : Sequelize.STRING,
		allowNull : false,
		validate : {
			len : [3,20]
		},
	/*	set(value){
			this.setDataValue('last_name', value.toLowerCase())
		}
*/
	},
    email:{
        type:Sequelize.STRING,
        allowNull:false,
        validate:{
            isEmail:true
        }
    },
    department:{
        type:Sequelize.STRING,
        allowNull:false,
        validate:{
            len:[3,30] 
        }
    },
},{
    underscored :true 
})


const Course=sequelize.define('course',{
    title:{
         type: Sequelize.STRING,
        allowNull:false,
        validate:{
            len:[5,20]
        }
    },
    shortDesc:{
         type: Sequelize.TEXT,
        allowNull:false,
        validate:{
            len:[10,250]
        }
    },
    startingDate:{
        type: Sequelize.DATE,
        allowNull:false,
        
    },
    examDeadline:{
        type: Sequelize.DATE,
        allowNull:false,
    
    }
})

Professor.hasMany(Course)
Course.hasMany(Student)

const app = express()
app.use(bodyParser.json())

app.get('/create', (req, res, next) => {
	sequelize.sync({force : true})
		.then(() => res.status(201).send('created'))
		.catch((err) => next(err))
})

app.get('/professors', (req, res, next) => {
	Professor.findAll()
		.then((professors) => res.status(200).send(professors))
		.catch((err) => next(err))
})
app.get('/courses', (req, res, next) => {
	Course.findAll()
		.then((result) => res.status(200).send(result))
		.catch((err) => next(err))
})
app.get('/students', (req, res, next) => {
	Student.findAll()
		.then((students) => res.status(200).send(students))
		.catch((err) => next(err))
})

app.post('/professors', (req, res, next) => {
	Professor.create(req.body)
		.then(() => res.status(201).send('professor created'))
		.catch((err) => next(err))
})

app.post('/students', (req, res, next) => {
	Student.create(req.body)
		.then(() => res.status(201).send('Student created'))
		.catch((err) => next(err))
})

app.post('/courses', (req, res, next) => {
	Course.create(req.body)
		.then(() => res.status(201).send('course created'))
		.catch((err) => next(err))
})

//get professors by name 
app.get('/professors/name/:name', (req, res, next) => {
  Professor.findAll({where: 
  {
    pfirst_name: req.params.name,
  }
  })
    .then((result) => {
      if (result){
        res.status(200).json(result)
      }
      else{
        res.status(404).send('Professor not found')
      }
    })
    .catch((error) => next(error))
})

//get courses by title
app.get('/courses/title/:name', (req, res, next) => {
  Course.findAll({where: 
  {
    title: req.params.name,
  }
  })
    .then((result) => {
      if (result){
        res.status(200).json(result)
      }
      else{
        res.status(404).send('Course not found')
      }
    })
    .catch((error) => next(error))
})

//get professors by id and all their courses
app.get('/professors/:id/courses', (req, res, next) => {
	Professor.findById(req.params.id, {include : [Course]})
		.then((result) => {
			if(result){
				res.status(200).json(result)
			}
			else{
				res.status(404).send('Professor not found')
			}
		})
		.catch((err) => next(err))	
})
//get the courses by id 
app.get('/courses/:id', (req, res) => {
	Course.findById(req.params.id)
		.then((result) => {
			if (result){
				res.status(200).json(result)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.catch(() => res.status(500).send('hm, that was unexpected...'))
})

//get courses by id and all their students
app.get('/courses/:id/students', (req, res, next) => {
	Course.findById(req.params.id)
		.then((course) => {
			if(course){
				return course.getStudents()
			}
			else{
				res.status(404).send('not found')
			}})
		.then((students) => res.status(200).send(students))
		.catch((err) => next(err))
})

//modify the courses
app.put('/courses/:id', (req, res,next) => {
	Course.findById(req.params.id)
		.then((result) => {
			if (result){
				return result.update(req.body)
			}
			else{
				res.status(404).send('not found')	
			}
		})
		.then(() => {
			res.status(201).send('modified')
		})
		.catch((error) => next(error))
})


//modify professors
app.put('/professors:id',(req,res,next)=>{
     Professor.findById(req.params.id)
    .then((professor)=>{
        if(professor){
            return professor.update(req.body, 
            {
                field:['pfirst_name', 'plast_name', 'email', 'department']
                
            })
            
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('modified')
        }
    })
    .catch((error)=>next(error))
})
//assings the course with cid to the professor with pid
app.put('/courses/:cid/professor/:pid',(req,res,next)=>{
     Course.findById(req.params.cid)
    .then((course)=>{
        if(course){
            return course.update(req.param.pid, 
            {
                field:['professor_id']
                
            })
            
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('modified')
        }
    })
    .catch((error)=>next(error))
})
//delete proffesor with id 
app.delete('/proffesors/:id',(req,res,next)=>{
      Professor.findById(req.params.id)
    .then((professor)=>{
        if(professor){
            return professor.destroy()
            
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('modified')
        }
    })
    .catch((error)=>next(error))
})

app.delete('/courses/:id',(req,res,next)=>{
      Course.findById(req.params.id)
    .then((courses)=>{
        if(courses){
            return courses.destroy()
            
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('modified')
        }
    })
    .catch((error)=>next(error))
})

app.delete('/students/:id',(req,res,next)=>{
      Course.findById(req.params.id)
    .then((students)=>{
        if(students){
            return students.destroy()
            
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then(()=>{
        if(!res.headersSent){
            res.status(201).send('modified')
        }
    })
    .catch((error)=>next(error))
})


app.get('/professor:pid/courses/:cid',(req,res,next)=>{
    Professor.findById(req.params.pid)
    .then((professor)=>{
        if(professor){
            res.status(200).json(professor)
        }
        else{
            res.status(404).send('not found')
        }
    })
    .then((courses)=>{
        if(!res.headersSent){
            res.status(201).send('created')
        }
    })
    .catch((error)=>next(error))
})



app.use((err,req,res,next)=>{
    console.warn(err)
    res.status(500).send('some error')
})

app.listen(8080)


