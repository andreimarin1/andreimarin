import React, { Component } from 'react';
import logo from './logo2.png';
import './App.css';
import ProfessorsList from './ProfessorsList'
import CourseList from './CourseList'
import AndroidList from './AndroidList'

class App extends Component {
  constructor(props){
    super(props)
    this.state={
      component: 1
    }
  }
  
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Simplest Elearning Platform</h1>
        </header>
        <body>
            
        <button className="button" type="button">LogIn with Facebook</button>
        <p>Available Courses</p>
        <input type="button" value="All courses" className="buttons" onClick={()=>{this.setState({component: 1})}}/>
        <input type="button" value="Android related courses" className="buttons" onClick={()=>{this.setState({component: 2})}}/>
        <input type="button" value="Our Team" className="buttons" onClick={()=>{this.setState({component: 3})}}/>
        <div>
        
        {this.state.component==1 && <CourseList/>}
        {this.state.component==2 && <AndroidList/>}
        {this.state.component==3 && <ProfessorsList/>}
        
        
        </div>
        </body>
      </div>
    );
  }
}

export default App;
