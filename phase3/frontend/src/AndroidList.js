import React, { Component } from 'react';
import AndroidStore from './store/AndroidStore'
import {EventEmitter} from 'fbemitter'

const emitter=new EventEmitter()
const store=new AndroidStore(emitter)

/*
let addProfessor=(professor)=>{
    store.createOne(professor)
}
*/




class AndroidList extends Component{
    constructor(props){
        super(props)
        this.state={
            courses:[],
            courseTitle : '',
            courseShortDesc : '',
            courseStartingDate:'',
            courseExamDeadline : '',
            courseProfId:''
            
            
        }
        this.handleChange =this.handleChange.bind(this)
    }
    componentDidMount(){
        store.getAll()
        console.log(store.content)
        emitter.addListener('ANDROID_LOAD', ()=>
        {
            this.setState({courses : store.content})
        })
    }
   
   handleChange(event){
       let target=event.target
       let value=target.value
       let name=target.name
       this.setState({
           [name] : value
       })
   }
   
    render(){
        return(
            <div>
            {
                this.state.courses.map((a)=>
                <div>
                <div>{'Title: '+a.title+'  Description: '+a.shortDesc}</div>
                <div>{'Starting date: '+a.startingDate+'   Exam Deadline '+a.examDeadline}</div>
                <iframe type="text/html" width="400" height="300" frameBorder="0" src={ "https://www.youtube.com/embed/"+ a.youtubeUrl +"?autoplay=0&controls=0"} />
                <div>
                <button type="button">Start Course!</button>
                <p></p>
                </div>
                </div>
                )
            }
            </div>
            )
    }
}

export default AndroidList;