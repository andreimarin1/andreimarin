import axios from 'axios'
const SERVER='https://andreim-nodejs-andreimarin1.c9users.io:8080'


class CourseStore{
    constructor(ee){
        this.emitter=ee
        this.content=[]
    }
    
    getAll(){
        axios(SERVER+'/courses')
        .then((respone)=>{
            this.content=respone.data
            this.emitter.emit('COURSE_LOAD')
        })
        .catch((error)=>console.warn(error))
    }
    createOne(Course){
        axios.post(SERVER+'/courses', Course)
            .then(()=>this.getAll())
            .catch((error)=>console.warn(error))
    }
}

export default CourseStore