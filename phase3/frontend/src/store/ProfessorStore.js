import axios from 'axios'
const SERVER='https://andreim-nodejs-andreimarin1.c9users.io:8080'


class ProfessorStore{
    constructor(ee){
        this.emitter=ee
        this.content=[]
    }
    
    getAll(){
        axios(SERVER+'/professors')
        .then((respone)=>{
            this.content=respone.data
            this.emitter.emit('PROFESSOR_LOAD')
        })
        .catch((error)=>console.warn(error))
    }
    createOne(Professor){
        axios.post(SERVER+'/professors', Professor)
            .then(()=>this.getAll())
            .catch((error)=>console.warn(error))
    }
}

export default ProfessorStore