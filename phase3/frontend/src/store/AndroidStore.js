import axios from 'axios'
const SERVER='https://andreim-nodejs-andreimarin1.c9users.io:8080'


class AndroidStore{
    constructor(ee){
        this.emitter=ee
        this.content=[]
    }
    
    getAll(){
        axios(SERVER+'/courses/title/android')
        .then((respone)=>{
            this.content=respone.data
            this.emitter.emit('ANDROID_LOAD')
        })
        .catch((error)=>console.warn(error))
    }
 
}

export default AndroidStore