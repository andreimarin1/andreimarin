import React, { Component } from 'react';
import ProfessorStore from './store/ProfessorStore'
import {EventEmitter} from 'fbemitter'

const emitter=new EventEmitter()
const store=new ProfessorStore(emitter)

/*
let addProfessor=(professor)=>{
    store.createOne(professor)
}
*/

class ProfessorsList extends Component{
    constructor(props){
        super(props)
        this.state={
            professors : [],
            professorName : '',
            professorEmail : ''
        }
        this.handleChange =this.handleChange.bind(this)
    }
    componentDidMount(){
        store.getAll()
        console.log(store.content)
        emitter.addListener('PROFESSOR_LOAD', ()=>
        {
            this.setState({professors : store.content})
        })
    }
   
   handleChange(event){
       let target=event.target
       let value=target.value
       let name=target.name
       this.setState({
           [name] : value
       })
   }
   
    render(){
        return(
            <div>
             <p>Professors</p>
            <div>
            {
                this.state.professors.map((a)=>
                <div>{a.pfirstName+' '+a.plastName+', Email:'+a.email}</div>
                )
            }
            </div>
              
            </div>
            )
    }
}

export default ProfessorsList;